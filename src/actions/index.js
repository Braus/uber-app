export const ADD_LOCATION = 'ADD_LOCATION';
export const FIND_PRODUCTS = 'FIND_PRODUCTS';

export const addLocation = (location) => ({
  type: ADD_LOCATION,
  payload: location
})

export const findProducts = (name) => ({
  type: FIND_PRODUCTS,
  payload: name
})
